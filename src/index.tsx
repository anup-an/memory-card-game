import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import Grid from './components/grid'
import './css/main.css';

  const App = () => {
    //generates random array of fruits
  let indices = ["apple", "orange", "banana", "grapes", "mango", "guava", "pear","jackfruit","apple", "orange", "banana", "grapes", "mango", "guava", "pear","jackfruit"];
  let wordArr:string[] = [];
  for (let i = indices.length;i--;){
    let random = indices.splice(Math.floor(Math.random()*(i+1)),1)[0];
    wordArr.push(random);
  }
  const [wordArray, setWordArray] = useState([...wordArr]);  

  return (
      
    <div className = "max-w-screen-sm m-auto my-5">
      <Grid words = {wordArray} />
    </div>

  )
}

ReactDOM.render(<App />,document.getElementById('root'));

