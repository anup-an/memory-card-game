import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import '../css/main.css';

interface PropInterface {words:string[]};
const Grid:React.FC<PropInterface> = (props) => {
const [displayArray,setDisplayArray] = useState(props.words.map((word,index) => <div className = "h-40 w-45 border shadow cursor-pointer rounded border-blue-500 m-1 text text-center" onClick = {() =>handleClick(index,word)}/>));

let [displays,tupleIndex,tupleWord,n] = [[...displayArray],[0,0],["",""],0];

const handleClick = (index:number,word:string) => {
    n = n + 1;
    if (n%2 === 1){
  
      tupleIndex[0] = index;
      tupleWord[0] = word;
      displays[index] = <div className = "h-40 w-45 border shadow cursor-pointer rounded bg-blue-500 border-blue-500 m-1 text text-center" onClick = {() =>handleClick(index,word)}/>
      setDisplayArray([...displays]);
  
    } else if (n%2 === 0){
      tupleIndex[1] = index;
      tupleWord[1] = word;
      console.log(tupleWord);
        let [index1,index2,word1,word2] = [tupleIndex[0],tupleIndex[1],tupleWord[0],tupleWord[1]];
      if (word1 === word2 && index1 !== index2 ){
        displays[index1] = <div className = "h-40 w-45 border shadow rounded bg-blue-500 border-blue-500 m-1 text text-center" ><div className = "m-12 py-2">{word1}</div></div>
  
        displays[index2] = <div className = "h-40 w-45 border shadow rounded bg-blue-500 border-blue-500 m-1 text text-center" ><div className = "m-12 py-2">{word2}</div></div>
  
        setDisplayArray([...displays]);
      }else if (word1 === word2 && index1 === index2) {
        displays[index1] = <div className = "h-40 w-45 border shadow cursor-pointer rounded border-blue-500 m-1 text text-center" onClick = {() =>handleClick(index1,word1)}/>
  
        displays[index2] = <div className = "h-40 w-45 border shadow cursor-pointer rounded border-blue-500 m-1 text text-center" onClick = {() =>handleClick(index2,word2)}/>
        setDisplayArray([...displays]);    
      }
      else if (word1 !== word2) {
      displays[index1] = <div className = "h-40 w-45 border shadow cursor-pointer rounded bg-green-500 border-blue-500 m-1 flex content-center" onClick = {() =>handleClick(index1,word1)}><div className = "m-12 py-2 text text-center">{word1}</div></div>
  
        displays[index2] = <div className = "h-40 w-45 border shadow cursor-pointer rounded bg-green-500 border-blue-500 m-1 flex content-center" onClick = {() =>handleClick(index2,word2)}><div className = "m-12 py-2">{word2}</div></div>
        setDisplayArray([...displays]);    
        timer(index1,index2,word1,word2);
      }
    }
  }

    const timer = (index1:number,index2:number,word1:string,word2:string) => {

    setTimeout(() => {
      displays[index1] = <div className = "h-40 w-45 border shadow cursor-pointer rounded border-blue-500 m-1 text text-center" onClick = {() =>handleClick(index1,word1)}></div>
  
        displays[index2] = <div className = "h-40 w-45 border shadow cursor-pointer rounded border-blue-500 m-1 text text-center" onClick = {() =>handleClick(index2,word2)}></div>
        setDisplayArray([...displays]);
    }, 1000);
  }

  //resets the game

  const resetGame = () => {
    displays = props.words.map((word,index) => <div className = "h-40 w-45 border shadow cursor-pointer rounded border-blue-500 m-1 text text-center" onClick = {() => handleClick(index,word)}/>)
    setDisplayArray([...displays])
  }

  

  return (
    <div>
      <div className="flex justify-center m-2" onClick = {() =>resetGame()}>
        <button className = "bg-red-500 border rounded px-2" >Reset Game</button>
      </div>
      <div className = "w-full border-2 rounded border-blue-500 bg-white-300 grid gap-0 md:grid-cols-4 ">
        {displayArray}     
      </div>
    </div>
  )
}

export default Grid;